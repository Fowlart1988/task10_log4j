package Experements;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.spi.LoggerContext;

public class App2 {

    // Applications using the Log4j 2 API will request a Logger with a specific name from the LogManager...

    // ...The Logger itself performs no direct actions. It simply has a name and is associated with a LoggerConfig.
    // It extends AbstractLogger and implements the required methods.
    // As the configuration is modified Loggers may become associated with a different LoggerConfig....

    //...with the logger name equal to the fully qualified name of the class.
    // This is a useful and straightforward method of defining loggers...
    private static final Logger logger1 = LogManager.getLogger("1");

    // Що дає rootloger???
    //private static final Logger logger = LogManager.getRootLogger();
    //Further information on the Logging API can be found in the Log4j 2 API.

    public static void main(final String... args) {



        // The LogManager will locate the appropriate LoggerContext and then obtain the Logger from it...
        // ...The LoggerContext acts as the anchor point for the Logging system.
        // ...However, it is possible to have multiple active LoggerContexts in an application
        // depending on the circumstances....
        LoggerContext loggerContext;




        // Every LoggerContext has an active Configuration. The Configuration contains all the Appenders,
        // context-wide Filters, LoggerConfigs and contains the reference to the StrSubstitutor.
        Configuration configuration;




        // If the Logger must be created it will be associated with the LoggerConfig that contains either
        // a) the same name as the Logger, b) the name of a parent package, or c) the root LoggerConfig.
        // LoggerConfig objects are created from Logger declarations in the configuration

        //The LoggerConfig contains a set of Filters that must
        //allow the LogEvent to pass before it will be passed to any Appenders
        LoggerConfig loggerConfig;



        // Filters that can be applied before control is passed to any LoggerConfig, after control is passed
        // to a LoggerConfig but before calling any Appenders, after control is passed to a LoggerConfig
        // but before calling a specific Appender, and on each Appender
        Filter filter;



        //The LoggerConfig is associated with the Appenders that actually deliver the LogEvents.

        //Currently, appenders exist for the console, files, remote socket servers, Apache Flume,
        // JMS, remote UNIX Syslog daemons, and various database APIs. See the section on Appenders
        // for more details on the various types available. More than one Appender can be attached to a Logger.
        Appender appender;

        // The Layout is responsible for formatting the LogEvent according to the user's wishes,
        // whereas an appender takes care of sending the formatted output to its destination.
        Layout<String> layout;

        logger1.info("info");
        logger1.trace("trace");
        logger1.debug("debug");
        logger1.error("error");
        logger1.warn("warning");
        logger1.fatal("fatal");
    }


}
